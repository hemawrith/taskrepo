public class Main {
    public static void main(String[] args) {

        Warrior Anny = new Warrior("Anny");
        Mage Katelynn = new Mage("Katelynn");
        Assassin Bill = new Assassin("Bill");
        Priest Sair = new Priest("Sair");

        Bill.makeSharp();

        Anny.takeDamage(Bill.getDamage());
        Sair.healHero("Anny", Anny.getHP());

    }
}
