public class Priest extends Player implements HealAbility {

    private int hp = 15000;
    private int damage = 1500;
    private String name;

    @Override
    public void entity() {
        System.out.println("I am a Priest!");
    }


    @Override
    public int getHP() {
        return hp;
    }

    @Override
    public void setHP(int hp) {
        this.hp = hp;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }


    public Priest(String name) {
        this.name=name;
        entity();
        System.out.println("My name is " + name + ".");
        System.out.println("My max HP is " + getHP() + " and my damage is " + getDamage() + "\n");
    }

    @Override
    public void healHero(String name, int heroHP) {
        heroHP += 2000;
        System.out.println(name + " is healed! HP now equals: " + heroHP + ".\n");

    }
}
