public interface HealAbility {
    void healHero(String name, int heroHP);
}
