public class Mage extends Player {

    private int hp = 12000;
    private int damage = 8000;
    private String name;

    @Override
    public void entity() {
        System.out.println("I am a Mage!");
    }

    @Override
    public int getHP() {
        return hp;
    }

    @Override
    public void setHP(int hp) {
        this.hp = hp;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    public Mage(String name) {
        this.name=name;
        entity();
        System.out.println("My name is " + name + ".");
        System.out.println("My max HP is " + getHP() + " and my damage is " + getDamage() + "\n");
    }

}
