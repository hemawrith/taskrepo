public class Assassin extends Player implements KnifeSharpening {

    private int hp = 17000;
    private int damage = 5000;
    private String name;

    @Override
    public void entity() {
        System.out.println("I am an Assassin!");

    }


    @Override
    public int getHP() {
        return hp;
    }

    @Override
    public void setHP(int hp) {
        this.hp = hp;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    public Assassin(String name) {
        this.name=name;
        entity();
        System.out.println("My name is " + name + ".");
        System.out.println("My max HP is " + getHP() + " and my damage is " + getDamage() + "\n");

    }

    @Override
    public void makeSharp() {
        damage += 2000;
        System.out.println("The fury makes the knife sharper! " + name + "'s damage increased and equals " + damage + ".\n");

    }


}
