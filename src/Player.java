public abstract class Player {

    public abstract void entity();

    public abstract void setHP(int hp);

    public abstract int getHP();

    public abstract void setDamage(int damage);

    public abstract int getDamage();

}
