public class Warrior extends Player {


    private int hp = 20000;
    private int damage = 1500;
    private String name;

    @Override
    public void entity() {
        System.out.println("I am a Warrior!");
    }


    @Override
    public int getHP() {
        return hp;
    }

    @Override
    public void setHP(int hp) {
        this.hp = hp;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    public Warrior(String name) {
        this.name=name;
        entity();
        System.out.println("My name is " + name + ".");
        System.out.println("My max HP is " + getHP() + " and my damage is " + getDamage() + "\n");

    }

    public void takeDamage(int damage){
        hp = hp-damage;
        System.out.println(name + " took damage! HP now equals: " + hp + ".\n");
    }
}
